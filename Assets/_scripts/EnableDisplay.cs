﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnableDisplay : MonoBehaviour
{
    [SerializeField] Canvas[] canvases;
    [SerializeField] Camera[] cameras;

    void Start()
    {
        if (Display.displays.Length >= 3)
        {
            for (int i = 0; i < Display.displays.Length; i++)
            {
                Display.displays[i].Activate();
            }
        }
        else {

//#if !UNITY_EDITOR

            for (int i = 0; i < canvases.Length; i++)
            {
                if(canvases[i] != null)
                    canvases[i].targetDisplay = 0;
            }
            for (int i = 0; i < cameras.Length; i++)
            {
                if (cameras[i] != null && i != 0) {
                    cameras[i].gameObject.SetActive(false);
                } else if (cameras[i] != null && i == 0) {
                    cameras[i].targetDisplay = 0;
                }
                
            }
//#endif

        }
    }

}
