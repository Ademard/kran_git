﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeightScript : MonoBehaviour
{
    [SerializeField] Camera cam;
    [SerializeField] HookScript[] hookScript;
    public float height = 100;
    [SerializeField] Transform curObjectMove;
    [SerializeField] LineRenderer lineRenderer;
    [SerializeField] Text heightText;
    private GameObject objectsToMooveCur;
    private Outline outline;
    private string curTag;

    [SerializeField] Color[] colorsLine;

    void FixedUpdate()
    {
        objectsToMooveCur = null;
        for (int i = 0; i < hookScript.Length; i++)
        {
            if (hookScript[i].objectsToMooveCur != null) {
                objectsToMooveCur = hookScript[i].objectsToMooveCur;
            }
        }

        if (objectsToMooveCur == null) {
            SetActiveHeight(false);
            height = 100;
            return;
        }

        RaycastHit hit;
        Ray downRay = new Ray(objectsToMooveCur.transform.position, -Vector3.up);

        // Cast a ray straight downwards.
        if (Physics.Raycast(downRay, out hit))
        {
            // The "error" in height is the difference between the desired height
            // and the height measured by the raycast distance.
            //float hoverError = hoverHeight - hit.distance;
            if (hit.transform.tag == "MoveFromHook" || hit.transform.tag == "Untagged" || hit.transform.tag == "targetPos") {
                curTag = hit.transform.tag;
                SetActiveHeight(true);
                height = (((int)(hit.distance * 100)) / 100f);
                //Debug.Log(hit.transform.tag);
                lineRenderer.SetPosition(0, objectsToMooveCur.transform.position);
                lineRenderer.SetPosition(1, new Vector3(hit.point.x, hit.point.y + 1, hit.point.z));
                heightText.text = height.ToString() + " м.";
                heightText.transform.parent.position = cam.WorldToScreenPoint(objectsToMooveCur.transform.position);
                if (outline == null) {
                    outline = objectsToMooveCur.GetComponent<Outline>();
                }
                SetColor();
            }
            
        }
    }

    private void SetActiveHeight(bool status) {
        //lineRenderer.enabled = status;
        heightText.transform.parent.gameObject.SetActive(status);
        if (!status) {
            outline = null;
        }
    }

    private void SetColor()
    {
        if (height > 0.5f)
        {
            lineRenderer.SetColors(colorsLine[0], colorsLine[1]);
            heightText.color = colorsLine[0];
            if (outline)
            {
                outline.OutlineColor = colorsLine[0];
            }
        }
        else {
            lineRenderer.SetColors(Color.red, colorsLine[1]);
            heightText.color = colorsLine[2];
            if (outline)
            {
                if (curTag != "targetPos")
                {
                    outline.OutlineColor = colorsLine[2];
                }
                else {
                    outline.OutlineColor = colorsLine[0];
                }
                
            }
        }
    }

    //сбрасываем все переносимые предметы при переходе к следующему модулю
    public void CleareObjectsToMoove() {
        for (int i = 0; i < hookScript.Length; i++)
        {
            if (hookScript[i].objectsToMooveCur != null)
            {
                hookScript[i].objectsToMooveCur = null;
            }
        }
        outline = null;
    }

}
