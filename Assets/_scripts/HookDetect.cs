﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HookDetect : MonoBehaviour
{
    [SerializeField] TrainingScript trainingScript;
    [SerializeField] HookScript hookScript;
    [SerializeField] int curHookNumber;
    public GameObject[] systemHookToMoove;


    public Transform planform, markerForStropal;

    private void Start()
    {
        markerForStropal.SetParent(planform);
    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.tag == "hook" && !systemHookToMoove[other.GetComponent<HookObject>().GetCurObjectID()].activeInHierarchy) {

            if (gameObject.tag != other.GetComponent<HookObject>().GetCurObjectBody().tag) {
                trainingScript.ShowWarning(true, trainingScript.warningMessages[2], 3);
                return;
            }
            hookScript.SetObjectToMoove(systemHookToMoove[other.GetComponent<HookObject>().GetCurObjectID()], other.GetComponent<HookObject>().GetCurObjectBody());

        }
        //else if (other.tag == "stropal") {
        //    hookScript.SetStropalshik(this.transform, other.transform.parent.GetComponent<HookObject>().GetCurObjectBody().transform, systemHookToMoove[other.transform.parent.GetComponent<HookObject>().GetCurObjectID()]);
        //    //hookScript.SetStropalshikStatus(true);
        //}
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "hook")
        {
            //Debug.Log("OnTriggerExit CleareObjectToMovove");
            hookScript.CleareObjectToMovove();
            
            //trainingScript.ShowWarning(false, trainingScript.warningMessages[2]);
        }
        //else if (other.tag == "stropal") {
        //    //hookScript.SetStropalshikStatus(false);
        //}
    }

}
