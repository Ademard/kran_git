﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HookObject : MonoBehaviour
{

    [SerializeField] int curObjectID;
    [SerializeField] GameObject curObject;

    public int GetCurObjectID() {
        return curObjectID;
    }

    public GameObject GetCurObjectBody()
    {
        return curObject;
    }
}
