﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HookScript : MonoBehaviour
{
    [SerializeField] StropalshikScript stropalshikScript;
    [SerializeField] Camera mainCamera;
    [SerializeField] GameObject[] PressHook;
    [SerializeField] Slider[] PressHookSlider;
    private Vector3 screenPosPressHook;

    //[SerializeField] GameObject[] systemHookToMoove;
    //[SerializeField] GameObject[] systemHookToMoove_1;

    [SerializeField] GameObject systemHookToMooveCur;
    public GameObject objectsToMooveCur;

    [SerializeField] Vector3 lastHookStartPos;
    string lastHookStartPosName;

    //private bool waitDoubleTap = true;

    //void Update()
    //{
    //if (systemHookToMooveCur != null)
    //{
    //    screenPosPressHook = mainCamera.WorldToScreenPoint(objectsToMooveCur.transform.position);
    //    PressHook.transform.position = screenPosPressHook;
    //}

    ////цепляем крюк
    //if (Input.GetKey(KeyCode.E) && waitDoubleTap)
    //{
    //    if (systemHookToMooveCur != null)
    //    {
    //        SetSystemHook(!systemHookToMooveCur.activeInHierarchy);
    //        StartCoroutine(WaitDoubleTap());
    //    }
    //}
    //}

    //устанавливаем текущий объект для зацепа 
    public void SetObjectToMoove(GameObject systemHookToMooveNew, GameObject curObject) {
        if (PressHook[0].activeInHierarchy && systemHookToMooveCur != null) {
            return;
        }
        systemHookToMooveCur = systemHookToMooveNew;
        objectsToMooveCur = curObject;
        SetPressHook(true);
        SetSystemHook(!systemHookToMooveCur.activeInHierarchy);
        
    }

    //очищаем текущий объект для зацепа
    public void CleareObjectToMovove()
    {
        //SetSystemHook(false);
        SetPressHook(false);
        if (objectsToMooveCur == null)
        {
            systemHookToMooveCur = null;
        }
        if (systemHookToMooveCur != null)
        {
            systemHookToMooveCur.transform.localPosition = lastHookStartPos;
        }
    }

    //выводим/скрываем уведомление о нажатии кнопки для зацепа
    private void SetPressHook(bool status) {
        for (int i = 0; i < PressHook.Length; i++)
        {
            PressHook[i].SetActive(status);
        }
        SetObjectOutline(status);
    }

    //цепляем/отцепляем объект
    private void SetSystemHook(bool status) {
        if (status)
        {
            StartCoroutine(WaitHooking(status));
        }
        else {
            systemHookToMooveCur.SetActive(status);
        }
    }

    //ждет пока крановщик зацепит
    private IEnumerator WaitHooking(bool status) {

        for (int i = 0; i < PressHookSlider.Length; i++)
        {
            PressHookSlider[i].value = 0;
        }

        Rigidbody rigidbody = objectsToMooveCur.GetComponent<Rigidbody>();

        if (lastHookStartPosName != systemHookToMooveCur.name)
        {
            lastHookStartPos = systemHookToMooveCur.transform.localPosition;
            lastHookStartPosName = systemHookToMooveCur.name;
        }


        //Debug.Log("while start: " + rigidbody.velocity.magnitude);
        while (PressHookSlider[0].value < PressHookSlider[0].maxValue && PressHook[0].activeInHierarchy)
        {
            
            for (int i = 0; i < PressHookSlider.Length; i++)
            {
                PressHookSlider[i].value = PressHookSlider[i].value + 1.0f;
            }

            //выравниваем крюк по центру объекта
            //systemHookToMooveCur.transform.position = Vector3.MoveTowards(systemHookToMooveCur.transform.position, new Vector3(objectsToMooveCur.transform.position.x, systemHookToMooveCur.transform.position.y * 2, objectsToMooveCur.transform.position.z), 50 * Time.deltaTime);
            //Debug.Log("while " + rigidbody.velocity.magnitude);
            yield return new WaitForEndOfFrame();
        }
        //Debug.Log("while end: " + rigidbody.velocity.magnitude);
        systemHookToMooveCur.transform.position = new Vector3(objectsToMooveCur.transform.position.x, systemHookToMooveCur.transform.position.y + 0.4f, objectsToMooveCur.transform.position.z);

        if (PressHookSlider[0].value == PressHookSlider[0].maxValue)
        {
            systemHookToMooveCur.SetActive(status);

            for (int i = 0; i < PressHook.Length; i++)
            {
                PressHook[i].SetActive(!status);
            }

            if (status && objectsToMooveCur != null)
            {

                //while (Vector3.Distance(systemHookToMooveCur.transform.position, systemHookStartPos.position) >= 0.0001f)
                //{
                //    //systemHookToMooveCur.transform.localPosition = lastHookStartPos;
                //    Debug.LogWarning(Vector3.Distance(systemHookToMooveCur.transform.position, systemHookStartPos.position));
                //    systemHookToMooveCur.transform.position = Vector3.MoveTowards(systemHookToMooveCur.transform.position, systemHookStartPos.position, 1 * Time.deltaTime);
                //}
                //systemHookToMooveCur.transform.localPosition = systemHookStartPos.localPosition;
                yield return new WaitForEndOfFrame();
                systemHookToMooveCur.transform.localPosition = lastHookStartPos;
                stropalshikScript.SetPos("targetPos");
                yield return new WaitForSeconds(3);
                StartCoroutine(WaitFinishPos(objectsToMooveCur));
            }
        }
        else {
            SetPressHook(false);
        }

    }

    //ждет пока крановщик отцепит
    private IEnumerator WaitHookingOut()
    {
        ObjectTouchScript objectTouchScript = objectsToMooveCur.GetComponent<ObjectTouchScript>();
        //Debug.Log("WaitHookingOut");

        while (!objectTouchScript.itsFinishPos)
        {
            yield return null;
        }

        for (int i = 0; i < PressHook.Length; i++)
        {
            PressHook[i].SetActive(true);
            PressHookSlider[i].value = 0;
        }

        Rigidbody rigidbody = objectsToMooveCur.GetComponent<Rigidbody>();

        while (rigidbody.velocity.magnitude >= 0.01)
        {
            if (!objectTouchScript.itsFinishPos)
            {
                StartCoroutine(WaitHookingOutNext());
                yield break;
            }
            yield return null;
        }

        while (PressHookSlider[0].value < PressHookSlider[0].maxValue)
        {
            
            for (int i = 0; i < PressHookSlider.Length; i++)
            {
                PressHookSlider[i].value = PressHookSlider[i].value + 1.0f;
            }

            if (!objectTouchScript.itsFinishPos)
            {

                StartCoroutine(WaitHookingOutNext());
                yield break;
            }
            yield return null;
        }

        if (PressHookSlider[0].value == PressHookSlider[0].maxValue)
        {
            systemHookToMooveCur.SetActive(false);

            if (objectsToMooveCur && objectsToMooveCur.GetComponent<Outline>()) {
                objectsToMooveCur.GetComponent<Outline>().enabled = false;
                if (objectsToMooveCur.transform.childCount >= 1) {
                    objectsToMooveCur.transform.GetChild(0).gameObject.SetActive(false);
                }
            }

            objectsToMooveCur = null;

            for (int i = 0; i < PressHook.Length; i++)
            {
                PressHook[i].SetActive(false);
            }
        }

        

        if (stropalshikScript.traversaOn)
        {
            stropalshikScript.SetPos("MoveFromTraversa");
        }
        else
        {
            stropalshikScript.SetPos("MoveFromHook");
        }

    }

    private IEnumerator WaitHookingOutNext()
    {
        for (int i = 0; i < PressHook.Length; i++)
        {
            PressHook[i].SetActive(false);
        }

        yield return new WaitForSeconds(0.5f);

        //Debug.LogWarning("Перезагружаем");

        StartCoroutine(WaitHookingOut());

    }

    //если зацепили груз, отслеживаем пока его не установят на нужное место
    private IEnumerator WaitFinishPos(GameObject objectToMooveCur) {
        ObjectTouchScript objectTouchScript = objectToMooveCur.GetComponent<ObjectTouchScript>();
        bool status = objectTouchScript.itsFinishPos;

        while (!status)
        {
            status = objectTouchScript.itsFinishPos;
            yield return null;
        }
        StartCoroutine(WaitHookingOut());
        //StartCoroutine(WaitHookingOutChecker());
    }

    //подсвечиваем выбранный объект
    private void SetObjectOutline(bool status) {
        if (objectsToMooveCur != null) {

            if (status)
            {
                objectsToMooveCur.GetComponent<Outline>().OutlineColor = Color.green;
            }
            else {
                objectsToMooveCur.GetComponent<Outline>().OutlineColor = Color.yellow;
            }
            //objectsToMooveCur.GetComponent<Outline>().enabled = status;
        }
    }

    ////делаем задержку между нажатиями E (что бы небыло повторных)
    //IEnumerator WaitDoubleTap() {
    //    waitDoubleTap = false;
    //    yield return new WaitForSeconds(1);
    //    waitDoubleTap = true;
    //}

    public void SetStropalshik(Transform hook, Transform objectToMove, GameObject systemHookToMoove) {
        stropalshikScript.SetInfoForStropalshik(hook, objectToMove, systemHookToMoove);
    }

    //public void SetStropalshikStatus(bool status)
    //{
    //    stropalshikScript.SetStropalshikStatus(status);
    //}

}
