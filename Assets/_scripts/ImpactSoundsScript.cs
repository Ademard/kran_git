﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImpactSoundsScript : MonoBehaviour
{

    [SerializeField] AudioClip impactClip;
    [SerializeField] AudioSource audioSourceImpact;
    [SerializeField] AudioSource audioSourceDruging;
    [SerializeField] Rigidbody rigidbody;

    //void Start()
    //{
    //    audioSource = GetComponent<AudioSource>();
    //}

    void OnCollisionEnter(Collision collide)
    {
        //Debug.Log("OnCollisionEnter: " + collide.gameObject.name);
        audioSourceImpact.PlayOneShot(impactClip, 0.8F);
    }

    private void OnCollisionStay(Collision collision)
    {
        //Debug.Log(collision.gameObject.name + ": " + rigidbody.velocity.magnitude);
        if (rigidbody.velocity.magnitude >= 0.1 && !audioSourceDruging.isPlaying && collision.gameObject.tag != "hook")
        {
            //Debug.Log("collision.gameObject.tag: " );
            audioSourceDruging.Play();
        }
        else if (rigidbody.velocity.magnitude <= 0.1 && audioSourceDruging.isPlaying) {
            audioSourceDruging.Stop();
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        audioSourceDruging.Stop();
    }

}
