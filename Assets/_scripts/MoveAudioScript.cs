﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveAudioScript : MonoBehaviour
{
    [SerializeField] AudioSource lebedkaLift;
    private int lebedkaLiftOldDirect = 3;

    [SerializeField] AudioSource lebedkaMove;
    private float lebedkaMovePitchDefault = 0.8f;
    private float lebedkaMoveVolumeDefault = 1;
    private int lebedkaMoveOldDirect = 3;

    [SerializeField] AudioSource craneMove;
    private float craneMovePitchDefault = 1;
    private float craneMoveVolumeDefault = 1;
    private int craneMoveOldDirect = 3;

    [SerializeField] AudioClip[] lebedkaAudios;
    [SerializeField] AudioClip[] craneAudios;

    [SerializeField] AudioSource clickSource;
    [SerializeField] AudioSource signalSource;

    public void SetLebedkaSoundLift(float newSpeed, float speedYcur, int curDirect) {
        if (newSpeed > 0 && speedYcur == 0 || curDirect != lebedkaLiftOldDirect && curDirect != 2)
        {
            lebedkaLift.clip = lebedkaAudios[0];
            lebedkaLift.Play();
        }
        else if (speedYcur > 0 && !lebedkaLift.isPlaying)
        {
            lebedkaLift.clip = lebedkaAudios[1];
            lebedkaLift.Play();
        }
        else if (newSpeed == 0 && speedYcur == 0 && lebedkaLift.isPlaying)
        {
            lebedkaLift.Stop();
        }
        lebedkaLiftOldDirect = curDirect;
    }

    public void SetLebedkaSoundMove(float newSpeed, float speedZcur, int curDirect)
    {
        if (newSpeed > 0 && speedZcur == 0 || curDirect != lebedkaMoveOldDirect && curDirect != 2)
        {
            lebedkaMove.clip = lebedkaAudios[0];
            lebedkaMove.pitch = lebedkaMovePitchDefault;
            lebedkaMove.volume = lebedkaMoveVolumeDefault;
            lebedkaMove.Play();
        }
        else if (speedZcur > 0 && !lebedkaMove.isPlaying)
        {
            lebedkaMove.clip = lebedkaAudios[1];
            lebedkaMove.Play();
        }
        else if (newSpeed == 0 && speedZcur == 0 && lebedkaMove.isPlaying)
        {
            lebedkaMove.Stop();
        }
        if (curDirect == 2)
        {
            lebedkaMove.pitch = Mathf.Clamp(speedZcur * 2, 0.5f, lebedkaMovePitchDefault);
            lebedkaMove.volume = Mathf.Clamp(speedZcur, 0, lebedkaMoveVolumeDefault);
        }

        lebedkaMoveOldDirect = curDirect;
    }

    public void SetCraneSoundMove(float newSpeed, float speedXcur, int curDirect)
    {
        if (newSpeed > 0 && speedXcur == 0 || curDirect != craneMoveOldDirect && curDirect != 2)
        {
            craneMove.clip = craneAudios[0];
            craneMove.pitch = craneMovePitchDefault;
            craneMove.volume = craneMoveVolumeDefault;
            craneMove.Play();
        }
        else if (speedXcur > 0 && !craneMove.isPlaying)
        {
            craneMove.clip = craneAudios[1];
            craneMove.Play();
        }
        else if (newSpeed == 0 && speedXcur == 0 && craneMove.isPlaying)
        {
            craneMove.Stop();
        }
        if (curDirect == 2)
        {
            craneMove.pitch = Mathf.Clamp(speedXcur * 2, 0.7f, craneMovePitchDefault);
            craneMove.volume = Mathf.Clamp(speedXcur, 0, craneMoveVolumeDefault);
        }

        craneMoveOldDirect = curDirect;
    }

    public void ClickSoundPlay() {
        clickSource.Play();
    }

    public void SignalPlay()
    {
        signalSource.Play();
    }
}
