﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveScript : MonoBehaviour
{
    [SerializeField] TimerScript timerScript;
    [SerializeField] TrainingScript trainingScript;
    public PanelScript panelScript;

    public Transform craneBeam, carriage;
    public Transform[] cableHook;

    //границы перемещения механизмов
    [SerializeField] Transform[] moovePointX;
    [SerializeField] Transform[] moovePointZ;
    public Transform[] moovePointY;

    public Transform[] hawsersLeft;
    public Transform[] hawsersRight;

    //запоминаем для плавного останова
    private int moovePointXcur;
    private int moovePointZcur;
    private int moovePointYcur;

    //скорость перемещения балки крана
    [SerializeField] float speedXmax;
    [SerializeField] float speedXcur;
    //скорость перемещения каретки крана
    [SerializeField] float speedZmax;
    [SerializeField] float speedZcur;
    //скорость перемещения крюков
    [SerializeField] float speedYmax;
    [SerializeField] float[] speedYcur;

    //параметр изменения скорости (плавность)
    [SerializeField] float speedDelta = 0.5f;
    [SerializeField] float speedDeltaY = 100;

    //параметр изменения скорости (плавность) при нажатии педали
    [SerializeField] float speedDeltaLeftPedal = 1.0f;
    [SerializeField] float speedDeltaRightPedal = 1.0f;

    [SerializeField] MoveAudioScript moveAudio;
    [SerializeField] Transform[] hooks;

    //переменные для работы с пультом
    [SerializeField] private bool RemoteControlUpY;
    [SerializeField] private bool RemoteControlDownY;

    private bool RemoteControlUpZ;
    private bool RemoteControlDownZ;

    private bool RemoteControlUpX;
    private bool RemoteControlDownX;

    private bool RemoteControlRightPedal;
    private bool RemoteControlLeftPedal;
    

    private void Start()
    {
        //panelScript.SetLight();
        speedYcur = new float[cableHook.Length];
    }

    void FixedUpdate()
    {
        //эмулятор нажатия педалей
        if (Input.GetKeyDown(KeyCode.Q)) {
            StartCoroutine(SetRemoteControlLeftPedal(1));
        }
        if (Input.GetKeyUp(KeyCode.Q))
        {
            StartCoroutine(SetRemoteControlLeftPedal(0));
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            StartCoroutine(SetRemoteControlRightPedal(1));
        }
        if (Input.GetKeyUp(KeyCode.E))
        {
            StartCoroutine(SetRemoteControlRightPedal(0));
        }


        if (!panelScript.PowerStatus || !timerScript.GetTimerIsRunning())
        {
            //Debug.Log("выключено питание или таймер не запущен");
            return;
        }

        //перемещаем балку крана
        if (Input.GetKey(KeyCode.D) || RemoteControlUpX)
        {
            moovePointXcur = 1;
            MooveCraneX(speedXmax, moovePointX[moovePointXcur].position, 1);
        }
        else if (Input.GetKey(KeyCode.A) || RemoteControlDownX)
        {
            moovePointXcur = 0;
            MooveCraneX(speedXmax, moovePointX[moovePointXcur].position, 0);
        }
        else {
            MooveCraneX(0, moovePointX[moovePointXcur].position, 2);
        }

        //перемещаем каретку крана
        if (Input.GetKey(KeyCode.W) || RemoteControlUpZ)
        {
            moovePointZcur = 1;
            MooveCraneZ(speedZmax, moovePointZ[moovePointZcur].position, 1);
        }
        else if (Input.GetKey(KeyCode.S) || RemoteControlDownZ)
        {
            moovePointZcur = 0;
            MooveCraneZ(speedZmax, moovePointZ[moovePointZcur].position, 0);
        }
        else {
            MooveCraneZ(0, moovePointZ[moovePointZcur].position, 2);
        }

        //перемещаем крючек крана

        if (Input.GetKey(KeyCode.UpArrow) || RemoteControlUpY)
        {
            //Debug.Log("RemoteControlUpY");
            moovePointYcur = 1;
            MooveCraneY(speedYmax, moovePointY[moovePointYcur].position, 1);
        }
        else if (Input.GetKey(KeyCode.DownArrow) || RemoteControlDownY)
        {
            moovePointYcur = 0;
            MooveCraneY(speedYmax, moovePointY[moovePointYcur].position, 0);
        }
        else {
            MooveCraneY(0, moovePointY[moovePointYcur].position, 2);
        }
    }

    //перемещаем балку крана
    private void MooveCraneX(float newSpeed, Vector3 curDirection, int curDirect) {

        if (RemoteControlLeftPedal)
        {
            newSpeed = 0;
            speedXcur = 0;
        }

        speedXcur = Mathf.Lerp(speedXcur, newSpeed, Time.deltaTime * speedDelta * speedDeltaLeftPedal);
        craneBeam.position = Vector3.MoveTowards(craneBeam.position, new Vector3(craneBeam.position.x, craneBeam.position.y, curDirection.z), speedXcur * Time.deltaTime);

        moveAudio.SetCraneSoundMove(newSpeed, speedXcur, curDirect);

        if (newSpeed > 0) {
            trainingScript.CheckMoveHeight();
        }
    }

    //перемещаем каретку крана
    private void MooveCraneZ(float newSpeed, Vector3 curDirection, int curDirect)
    {
        if (RemoteControlLeftPedal)
        {
            newSpeed = 0;
            speedZcur = 0;
        }

        speedZcur = ((int)(Mathf.Lerp(speedZcur, newSpeed, Time.deltaTime * speedDelta * speedDeltaLeftPedal) * 1000)) / 1000f;
        carriage.position = Vector3.MoveTowards(carriage.position, new Vector3(curDirection.x, carriage.position.y, carriage.position.z), speedZcur * Time.deltaTime);

        moveAudio.SetLebedkaSoundMove(newSpeed, speedZcur, curDirect);

        if (newSpeed > 0)
        {
            trainingScript.CheckMoveHeight();
        }
    }

    //перемещаем крючек крана
    private void MooveCraneY(float newSpeed, Vector3 curDirection, int curDirect)
    {
        float speedYcurLocal = 0;
        float newSpeedLocal;

        if (RemoteControlRightPedal)
        {
            newSpeed = 0;
        }

        for (int i = 0; i < panelScript.GetHooksCount(); i++)
        {
            newSpeedLocal = newSpeed;

            //если крюк выключен или поднят слишком высоко (ограничиваем перемещение)
            if (!panelScript.GetHooksStatus(i) || hooks[i].position.y >= curDirection.y && curDirect == 1) {
                newSpeedLocal = 0;
            }
            
            speedYcur[i] = ((int)(Mathf.Lerp(speedYcur[i], newSpeedLocal, Time.deltaTime * speedDeltaY * speedDeltaRightPedal) * 1000)) / 1000;
            //cableHook[i].position = Vector3.MoveTowards(cableHook[i].position, new Vector3(cableHook[i].position.x, curDirection.y, cableHook[i].position.z), speedYcur[i] * Time.deltaTime);

            if (curDirect == 0) {
                curDirect = -1;
            }

            hawsersLeft[i].Rotate(curDirect * Vector3.forward * (speedYcur[i] * Time.deltaTime));
            hawsersRight[i].Rotate(curDirect * Vector3.back * (speedYcur[i] * Time.deltaTime));

            if (speedYcur[i] > speedYcurLocal)
            {
                speedYcurLocal = speedYcur[i];
            }

        }

        moveAudio.SetLebedkaSoundLift(newSpeed, speedYcurLocal, curDirect);
    }

    //устанавливаем скорость подъема крюка
    public void SetHookSpeed(float newSpeed) {
        StartCoroutine(SetHookSpeedIE(newSpeed));
    }

    //устанавливаем скорость крюков с панели
    public IEnumerator SetHookSpeedIE(float newSpeed)
    {
        speedYmax = (newSpeed + 1) * 100;
        yield return null;
    }

    //получаем с пульта данные подъема крюка вверх
    public IEnumerator SetRemoteControlUpY(int status) {
        RemoteControlUpY = (status > 0) ? true : false;
        yield return null;
    }

    //получаем с пульта данные опускания крюка вниз
    public IEnumerator SetRemoteControlDownY(int status)
    {
        RemoteControlDownY = (status > 0) ? true : false;
        yield return null;
    }

    //получаем с пульта данные для перемещения каретки Z+
    public IEnumerator SetRemoteControlUpZ(int status)
    {
        RemoteControlUpZ = (status > 0) ? true : false;
        Debug.Log("RemoteControlUpZ: " + RemoteControlUpZ);
        yield return null;
    }

    //получаем с пульта данные для перемещения каретки Z-
    public IEnumerator SetRemoteControlDownZ(int status)
    {
        RemoteControlDownZ = (status > 0) ? true : false;
        Debug.Log("RemoteControlDownZ: " + RemoteControlDownZ);
        yield return null;
    }

    //получаем с пульта данные для перемещения балки X+
    public IEnumerator SetRemoteControlUpX(int status)
    {
        RemoteControlUpX = (status > 0) ? true : false;
        Debug.Log("RemoteControlUpX: " + RemoteControlUpX);
        yield return null;
    }

    //получаем с пульта данные для перемещения балки X-
    public IEnumerator SetRemoteControlDownX(int status)
    {
        RemoteControlDownX = (status > 0) ? true : false;
        Debug.Log("RemoteControlDownX: " + RemoteControlDownX);
        yield return null;
    }

    //получаем с пульта данные нажатия правой педали
    public IEnumerator SetRemoteControlRightPedal(int status)
    {
        RemoteControlRightPedal = (status > 0) ? true : false;

        if (RemoteControlRightPedal)
        {
            speedDeltaRightPedal = 3.0f;
        }
        else
        {
            speedDeltaRightPedal = 1.0f;
        }

        yield return null;
    }

    //получаем с пульта данные нажатия левой педали
    public IEnumerator SetRemoteControlLeftPedal(int status)
    {
        RemoteControlLeftPedal = (status > 0) ? true : false;

        if (RemoteControlLeftPedal)
        {
            speedDeltaLeftPedal = 3.0f;
        }
        else
        {
            speedDeltaLeftPedal = 1.0f;
        }

        yield return null;
    }

    //выбираем текущее захватывающее устройство
    public IEnumerator SetCurDevice(int status)
    {
        if (status == 1)
        {
            trainingScript.SetTraversaVisibleCheck(true);
        }
        else {
            trainingScript.SetTraversaVisibleCheck(false);
        }
        yield return null;
    }

}
