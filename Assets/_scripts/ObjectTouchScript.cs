﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectTouchScript : MonoBehaviour
{

    [SerializeField] TrainingScriptCurParameters trainingCur;
    public bool itsFinishPos;
    private bool fierstEnable;

    IEnumerator Start()
    {
        fierstEnable = true;
        yield return new WaitForSeconds(2);
        fierstEnable = false;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (fierstEnable) {
            return;
        }
        if (collision.gameObject.tag == "targetPos")
        {
            trainingCur.SetPositionCount(1);
            itsFinishPos = true;
        }
        else {

            if (collision.gameObject.tag != "MoveFromHook" && collision.gameObject.tag != "targetPos")
            {
                trainingCur.trainingScript.SetImpactWarning();
            }
        }
    }

    void OnCollisionExit(Collision collision)
    {
        if (fierstEnable)
        {
            return;
        }
        if (collision.gameObject.tag == "targetPos")
        {
            trainingCur.SetPositionCount(-1);
            itsFinishPos = false;
        }
    }

}
