﻿using AOT;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static RemoteControlScript;

public class PanelScript : MonoBehaviour
{

    [SerializeField] MoveScript moveScript;
    [SerializeField] MoveAudioScript audioScript;

    //[SerializeField] Toggle panelToggleEnable;
    //[SerializeField] GameObject panel;
    //[SerializeField] bool panelStatus;

    [SerializeField] Toggle powerToggle;
    public bool PowerStatus;

    [SerializeField] Toggle lightKeyToggle;
    [SerializeField] GameObject[] lightPulleyCran;
    [SerializeField] bool lightStatus;

    [SerializeField] Toggle craneCheckToggle;
    [SerializeField] bool craneCheckStatus;

    [SerializeField] Toggle[] hooksToggle;
    [SerializeField] bool[] hooksStatus;

    [SerializeField] Slider hookSpeedSlider;

    [SerializeField] Slider selectDeviceSlider;

    void Start()
    {
        hooksStatus = new bool[2];
    }

    ////вкл/выкл панель
    //public void SetPanelVisible() {
    //    panelStatus = !panelStatus;
    //    panel.SetActive(panelStatus);
    //    panelToggleEnable.isOn = panelStatus;
    //    audioScript.ClickSoundPlay();
    //}

    //вкл/выкл Питание
    public void SetPower()
    {
        powerToggle.isOn = !PowerStatus;
        StartCoroutine(SetPowerIE((powerToggle.isOn == true) ? 1 : 0));
    }

    public IEnumerator SetPowerIE(int status)
    {
        PowerStatus = (status == 1) ? true : false;
        powerToggle.isOn = PowerStatus;
        SetHookSpeed();
        audioScript.ClickSoundPlay();
        yield return null;
    }

    //вкл/выкл Света
    public void SetLight()
    {
        StartCoroutine(SetLightIE());
    }

    public IEnumerator SetLightIE()
    {
        lightStatus = !lightStatus;
        //lightKeyToggle.isOn = lightStatus;

        for (int i = 0; i < lightPulleyCran.Length; i++)
        {
            if (lightPulleyCran[i] != null)
            {
                lightPulleyCran[i].SetActive(lightStatus);
            }
        }
        audioScript.ClickSoundPlay();
        yield return null;
    }

    //проверка крана
    public void CraneCheck()
    {
        StartCoroutine(CraneCheckIE());
    }

    public IEnumerator CraneCheckIE()
    {
        craneCheckStatus = true;
        //Debug.Log("TRUE!");
        audioScript.ClickSoundPlay();
        yield return null;
    }

    //вкл/выкл Крюк №1
    public void SetHook1()
    {
        StartCoroutine(SetHook1IE());
    }

    public IEnumerator SetHook1IE()
    {
        hooksStatus[0] = !hooksStatus[0];
        hooksToggle[0].isOn = hooksStatus[0];
        audioScript.ClickSoundPlay();
        yield return hooksStatus[0];
    }

    //вкл/выкл Крюк №2
    public void SetHook2()
    {
        StartCoroutine(SetHook2IE());
    }

    public IEnumerator SetHook2IE()
    {
        hooksStatus[1] = !hooksStatus[1];
        hooksToggle[1].isOn = hooksStatus[1];
        audioScript.ClickSoundPlay();
        yield return null;
    }

    //устанавливаем скорость крюков
    public void SetHookSpeed() {
        moveScript.SetHookSpeed(hookSpeedSlider.value);
        audioScript.ClickSoundPlay();
    }

    //аварийное выключение
    public void ErrorStop() {
        StartCoroutine(ErrorStopIE());
    }

    public IEnumerator ErrorStopIE()
    {
        PowerStatus = false;
        powerToggle.isOn = PowerStatus;
        audioScript.ClickSoundPlay();
        yield return null;
    }

    //получаем статусы включения крюков
    public bool GetHooksStatus(int curHook) {
        return hooksStatus[curHook];
    }

    //получаем количество крюков
    public int GetHooksCount()
    {
        return hooksStatus.Length;
    }

    //сигнал
    public void Signal() {
        StartCoroutine(SignalIE());
    }

    public IEnumerator SignalIE()
    {
        audioScript.SignalPlay();
        yield return null;
    }

    //вкл/выкл устройства с виртуальной панели
    public void SetDevice()
    {
        StartCoroutine(moveScript.SetCurDevice((int)selectDeviceSlider.value));
    }

}
