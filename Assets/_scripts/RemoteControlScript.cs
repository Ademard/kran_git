﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using System;
using AOT;

public class RemoteControlScript : MonoBehaviour
{
    /*Дані, що отримані з пульта через Call-Back виклики. Ці виклики передають у програму покажчик на дані,
      що можуть бути різнородними, тому потрібно додатково приводити дані до потрібного типу.*/
    public delegate void CallBackRecvData(IntPtr data);

    //Вставлює Call-Back для правого джойстика, вісь +Y
    [DllImport("CraneAPI", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, ExactSpelling = false, EntryPoint = "setRecvRightStickY")]
    public static extern void setRecvRightStickY(CallBackRecvData cb);
    //Вставлює Call-Back для правого джойстика, вісь -Y
    [DllImport("CraneAPI", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, ExactSpelling = false, EntryPoint = "setRecvRightStickMinusY")]
    public static extern void setRecvRightStickMinusY(CallBackRecvData cb);
    //Вставлює Call-Back для правого джойстика, вісь X
    [DllImport("CraneAPI", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, ExactSpelling = false, EntryPoint = "setRecvRightStickX")]
    public static extern void setRecvRightStickX(CallBackRecvData cb);
    //Вставлює Call-Back для правого джойстика, вісь -X
    [DllImport("CraneAPI", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, ExactSpelling = false, EntryPoint = "setRecvRightStickMinusX")]
    public static extern void setRecvRightStickMinusX(CallBackRecvData cb);
    //Вставлює Call-Back для лівого джойстика, вісь +Y
    [DllImport("CraneAPI", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, ExactSpelling = false, EntryPoint = "setRecvLeftStickY")]
    public static extern void setRecvLeftStickY(CallBackRecvData cb);
    //Вставлює Call-Back для лівого джойстика, вісь -Y
    [DllImport("CraneAPI", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, ExactSpelling = false, EntryPoint = "setRecvLeftStickMinusY")]
    public static extern void setRecvLeftStickMinusY(CallBackRecvData cb);
    //Вставлює Call-Back для лівого джойстика, вісь X
    [DllImport("CraneAPI", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, ExactSpelling = false, EntryPoint = "setRecvLeftStickX")]
    public static extern void setRecvLeftStickX(CallBackRecvData cb);
    //Вставлює Call-Back для лівого джойстика, вісь -X
    [DllImport("CraneAPI", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, ExactSpelling = false, EntryPoint = "setRecvLeftStickMinusX")]
    public static extern void setRecvLeftStickMinusX(CallBackRecvData cb);
    //Вставлює Call-Back для лівої педалі
    [DllImport("CraneAPI", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, ExactSpelling = false, EntryPoint = "setRecvLeftPedal")]
    public static extern void setRecvLeftPedal(CallBackRecvData cb);
    //Вставлює Call-Back для правої педалі
    [DllImport("CraneAPI", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, ExactSpelling = false, EntryPoint = "setRecvRightPedal")]
    public static extern void setRecvRightPedal(CallBackRecvData cb);
    //Вставлює Call-Back для кнопки освітлення
    [DllImport("CraneAPI", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, ExactSpelling = false, EntryPoint = "setRecvLightingButton")]
    public static extern void setRecvLightingButton(CallBackRecvData cb);
    //Вставлює Call-Back для кнопки вибору 2го крюка
    [DllImport("CraneAPI", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, ExactSpelling = false, EntryPoint = "setRecvSelect2CrookButton")]
    public static extern void setRecvSelect2CrookButton(CallBackRecvData cb);
    //Вставлює Call-Back для кнопки вибору 1го крюка
    [DllImport("CraneAPI", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, ExactSpelling = false, EntryPoint = "setRecvSelect1CrookButton")]
    public static extern void setRecvSelect1CrookButton(CallBackRecvData cb);
    //Вставлює Call-Back для кнопки перевірки
    [DllImport("CraneAPI", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, ExactSpelling = false, EntryPoint = "setRecvCheckButton")]
    public static extern void setRecvCheckButton(CallBackRecvData cb);
    //Вставлює Call-Back для перемикача вибору пристрою
    [DllImport("CraneAPI", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, ExactSpelling = false, EntryPoint = "setRecvSelectionSwitch")]
    public static extern void setRecvSelectionSwitch(CallBackRecvData cb);
    //Вставлює Call-Back для перемикача вибору швидкості
    [DllImport("CraneAPI", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, ExactSpelling = false, EntryPoint = "setRecvSwitchSpeedWinches")]
    public static extern void setRecvSwitchSpeedWinches(CallBackRecvData cb);
    //Вставлює Call-Back для ключа
    [DllImport("CraneAPI", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, ExactSpelling = false, EntryPoint = "setRecvKey")]
    public static extern void setRecvKey(CallBackRecvData cb);
    //Встановлює Call-Back для сигналу
    [DllImport("CraneAPI", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, ExactSpelling = false, EntryPoint = "setRecvSignal")]
    public static extern void setRecvSignal(CallBackRecvData cb);
    //Встановлює Call-Back для аварійного вимкнення
    [DllImport("CraneAPI", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, ExactSpelling = false, EntryPoint = "setRecvEmergencyShutdown")]
    public static extern void setRecvEmergencyShutdown(CallBackRecvData cb);
    //Запуск прийому даних у паралельному потоці (_com - номер ком-порта починаючи з 0!!!, _speed - швидкість обміну, int _delay - затримка між опитуваннями ком-порта у мс)
    //У разі успішного запуску повертається 0
    [DllImport("CraneAPI", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, ExactSpelling = false, EntryPoint = "StartReceiveLoop")]
    public static extern Int16 StartReceiveLoop(Int16 _com, Int16 _speed, Int16 _delay);
    //Зупинка циклу прийому і закриття ком-порта
    [DllImport("CraneAPI", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, ExactSpelling = false, EntryPoint = "Close")]
    public static extern void Close(Int16 _сom);
    //Керування лампою живлення
    [DllImport("CraneAPI", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, ExactSpelling = false, EntryPoint = "setSendPowerLamp")]
    public static extern void setSendPowerLamp(bool value);
    //Керування лампою вибору 1го крюка
    [DllImport("CraneAPI", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, ExactSpelling = false, EntryPoint = "setSendSelect1CrookLamp")]
    public static extern void setSendSelect1CrookLamp(bool value);
    //Керування лампою вибору 2го крюка
    [DllImport("CraneAPI", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, ExactSpelling = false, EntryPoint = "setSendSelect2CrookLamp")]
    public static extern void setSendSelect2CrookLamp(bool value);
    //Керування лампою несправності ламп
    [DllImport("CraneAPI", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, ExactSpelling = false, EntryPoint = "setSendBrakeFaultLamp")]
    public static extern void setSendBrakeFaultLamp(bool value);

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    //Далі наведемо приклади реалізації Call-Back функцій
    [MonoPInvokeCallback(typeof(CallBackRecvData))]
    private static void RecvRightStickY(IntPtr data)
    {
        Byte[] value = new Byte[1];
        Marshal.Copy(data, value, 0, 1);
        //Debug.LogError($"RecvRightStickY = {value[0]:X}");

        UnityMainThreadDispatcher.Instance().Enqueue(moveScript.SetRemoteControlUpY(value[0]));
    }

    [MonoPInvokeCallback(typeof(CallBackRecvData))]
    private static void RecvRightStickMinusY(IntPtr data)
    {
        Byte[] value = new Byte[1];
        Marshal.Copy(data, value, 0, 1);
        //Debug.LogError($"RecvRightStickMinusY = {value[0]:X}");

        UnityMainThreadDispatcher.Instance().Enqueue(moveScript.SetRemoteControlDownY(value[0]));
    }

    [MonoPInvokeCallback(typeof(CallBackRecvData))]
    private static void RecvRightStickX(IntPtr data)
    {
        Byte[] value = new Byte[1];
        Marshal.Copy(data, value, 0, 1);
        //Debug.LogError($"RecvRightStickX = {value[0]:X}");
    }

    [MonoPInvokeCallback(typeof(CallBackRecvData))]
    private static void RecvRightStickMinusX(IntPtr data)
    {
        Byte[] value = new Byte[1];
        Marshal.Copy(data, value, 0, 1);
        //Debug.LogError($"RecvRightStickMinusX = {value[0]:X}");
    }

    [MonoPInvokeCallback(typeof(CallBackRecvData))]
    private static void RecvLeftStickY(IntPtr data)
    {
        Byte[] value = new Byte[1];
        Marshal.Copy(data, value, 0, 1);
        //Debug.LogError($"RecvLeftStickY = {value[0]:X}");

        UnityMainThreadDispatcher.Instance().Enqueue(moveScript.SetRemoteControlUpZ(value[0]));
    }

    [MonoPInvokeCallback(typeof(CallBackRecvData))]
    private static void RecvLeftStickMinusY(IntPtr data)
    {
        Byte[] value = new Byte[1];
        Marshal.Copy(data, value, 0, 1);
        //Debug.LogError($"RecvLeftStickMinusY = {value[0]:X}");

        UnityMainThreadDispatcher.Instance().Enqueue(moveScript.SetRemoteControlDownZ(value[0]));
    }

    [MonoPInvokeCallback(typeof(CallBackRecvData))]
    private static void RecvLeftStickX(IntPtr data)
    {
        Byte[] value = new Byte[1];
        Marshal.Copy(data, value, 0, 1);
        //Debug.LogError($"RecvLeftStickX = {value[0]:X}");

        UnityMainThreadDispatcher.Instance().Enqueue(moveScript.SetRemoteControlUpX(value[0]));
    }

    [MonoPInvokeCallback(typeof(CallBackRecvData))]
    private static void RecvLeftStickMinusX(IntPtr data)
    {
        Byte[] value = new Byte[1];
        Marshal.Copy(data, value, 0, 1);
        //Debug.LogError($"RecvLeftStickMinusX = {value[0]:X}");

        UnityMainThreadDispatcher.Instance().Enqueue(moveScript.SetRemoteControlDownX(value[0]));
    }

    [MonoPInvokeCallback(typeof(CallBackRecvData))]
    private static void RecvLeftPedal(IntPtr data)
    {
        Byte[] value = new Byte[1];
        Marshal.Copy(data, value, 0, 1);
        Debug.LogError($"RecvLeftPedal = {value[0]}");

        UnityMainThreadDispatcher.Instance().Enqueue(moveScript.SetRemoteControlLeftPedal(value[0]));
    }

    [MonoPInvokeCallback(typeof(CallBackRecvData))]
    private static void RecvRightPedal(IntPtr data)
    {
        Byte[] value = new Byte[1];
        Marshal.Copy(data, value, 0, 1);
        Debug.LogError($"RecvRightPedal = {value[0]}");

        UnityMainThreadDispatcher.Instance().Enqueue(moveScript.SetRemoteControlRightPedal(value[0]));
    }

    [MonoPInvokeCallback(typeof(CallBackRecvData))]
    private static void RecvLightingButton(IntPtr data)
    {
        Byte[] value = new Byte[1];
        Marshal.Copy(data, value, 0, 1);
        Debug.LogError($"RecvLightingButton = {value[0]}");

        if (value[0] == 1) {
            UnityMainThreadDispatcher.Instance().Enqueue(moveScript.panelScript.SetLightIE());
        }
    }

    [MonoPInvokeCallback(typeof(CallBackRecvData))]
    private static void RecvSelect2CrookButton(IntPtr data)
    {
        Byte[] value = new Byte[1];
        Marshal.Copy(data, value, 0, 1);
        Debug.LogError($"RecvSelect2CrookButton = {value[0]}");

        if (value[0] == 1) {
            UnityMainThreadDispatcher.Instance().Enqueue(moveScript.panelScript.SetHook2IE());
            remoteControlScript.SetCrookLamp2();
        }
    }

    [MonoPInvokeCallback(typeof(CallBackRecvData))]
    private static void RecvSelect1CrookButton(IntPtr data)
    {
        Byte[] value = new Byte[1];
        Marshal.Copy(data, value, 0, 1);
        Debug.LogError($"RecvSelect1CrookButton = {value[0]}");

        if (value[0] == 1)
        {
            UnityMainThreadDispatcher.Instance().Enqueue(moveScript.panelScript.SetHook1IE());
            remoteControlScript.SetCrookLamp1();
        }

        
    }

    [MonoPInvokeCallback(typeof(CallBackRecvData))]
    private static void RecvCheckButton(IntPtr data)
    {
        Byte[] value = new Byte[1];
        Marshal.Copy(data, value, 0, 1);
        Debug.LogError($"RecvCheckButton = {value[0]}");

        if (value[0] == 1)
        {
            UnityMainThreadDispatcher.Instance().Enqueue(moveScript.panelScript.CraneCheckIE());
        }
    }

    [MonoPInvokeCallback(typeof(CallBackRecvData))]
    private static void RecvSelectionSwitch(IntPtr data)
    {
        Byte[] value = new Byte[1];
        Marshal.Copy(data, value, 0, 1);
        Debug.LogError($"RecvSelectionSwitch = {value[0]}");

        UnityMainThreadDispatcher.Instance().Enqueue(moveScript.SetCurDevice(value[0]));
    }

    [MonoPInvokeCallback(typeof(CallBackRecvData))]
    private static void RecvSwitchSpeedWinches(IntPtr data)
    {
        Byte[] value = new Byte[1];
        Marshal.Copy(data, value, 0, 1);
        Debug.LogError($"RecvSwitchSpeedWinches = {value[0]}");

        UnityMainThreadDispatcher.Instance().Enqueue(moveScript.SetHookSpeedIE(value[0]));
    }

    [MonoPInvokeCallback(typeof(CallBackRecvData))]
    private static void RecvKey(IntPtr data)
    {
        Byte[] value = new Byte[1];
        Marshal.Copy(data, value, 0, 1);
        Debug.LogError($"RecvKey = {value[0]}");

        UnityMainThreadDispatcher.Instance().Enqueue(moveScript.panelScript.SetPowerIE(value[0]));
        //setSendPowerLamp((value[0] > 0) ? true : false);
        remoteControlScript.SetPowerLamp((value[0] > 0) ? true : false);
    }

    [MonoPInvokeCallback(typeof(CallBackRecvData))]
    private static void RecvSignal(IntPtr data)
    {
        Byte[] value = new Byte[1];
        Marshal.Copy(data, value, 0, 1);
        Debug.LogError($"RecvSignal = {value[0]}");

        UnityMainThreadDispatcher.Instance().Enqueue(moveScript.panelScript.SignalIE());
    }

    [MonoPInvokeCallback(typeof(CallBackRecvData))]
    private static void RecvEmergencyShutdown(IntPtr data)
    {
        Byte[] value = new Byte[1];
        Marshal.Copy(data, value, 0, 1);
        Debug.LogError($"RecvEmergencyShutdown = {value[0]}");

        if (value[0] == 1) {
            UnityMainThreadDispatcher.Instance().Enqueue(moveScript.panelScript.ErrorStopIE());
        }
    }

    void Start()
    {
        //Встановлюємо відповідні Call-Back виклики
        setRecvRightStickY(RecvRightStickY);
        setRecvRightStickMinusY(RecvRightStickMinusY);
        setRecvRightStickX(RecvRightStickX);
        setRecvRightStickMinusX(RecvRightStickMinusX);
        setRecvLeftStickY(RecvLeftStickY);
        setRecvLeftStickMinusY(RecvLeftStickMinusY);
        setRecvLeftStickX(RecvLeftStickX);
        setRecvLeftStickMinusX(RecvLeftStickMinusX);
        setRecvLeftPedal(RecvLeftPedal);
        setRecvRightPedal(RecvRightPedal);
        setRecvLightingButton(RecvLightingButton);
        setRecvSelect2CrookButton(RecvSelect2CrookButton);
        setRecvSelect1CrookButton(RecvSelect1CrookButton);
        setRecvCheckButton(RecvCheckButton);
        setRecvSelectionSwitch(RecvSelectionSwitch);
        setRecvSwitchSpeedWinches(RecvSwitchSpeedWinches);
        setRecvKey(RecvKey);
        setRecvSignal(RecvSignal);
        setRecvEmergencyShutdown(RecvEmergencyShutdown);
        //Відкриваємо порт 0 (нумерація починається з 0!!!)

        

        if (StartReceiveLoop(GetComPort(), 19200, 1) == 0)
        {
            Debug.LogWarning("Port open successfully!");
        }
        moveScript = GetComponent<MoveScript>();
        remoteControlScript = this;
    }


    public static MoveScript moveScript;
    public static RemoteControlScript remoteControlScript;

    private void OnDestroy()
    {
        //Закриваємо порт 2
        Close(6);
    }

    private short GetComPort()
    {
        if (PlayerPrefs.HasKey("comPort"))
        {
            return Convert.ToInt16(PlayerPrefs.GetInt("comPort"));
        }
        else
        {
            return 0;
        }
    }

    private void SetPowerLamp(bool status)
    {
        setSendPowerLamp(status);
        //Debug.Log("включили лампу питания");
    }

    private void SetCrookLamp1()
    {
        setSendSelect1CrookLamp(select1CrookLamp = !select1CrookLamp);
        //Debug.Log("включили лампу крюка 1");
    }

    private void SetCrookLamp2()
    {
        setSendSelect2CrookLamp(select2CrookLamp = !select2CrookLamp);
        //Debug.Log("включили лампу крюка 2");
    }

    //private bool powerLamp = false;
    private bool select1CrookLamp = false;
    private bool select2CrookLamp = false;
    private bool brakeFaultLamp = false;

    //void Update()
    //{
    //    if (Input.GetKeyDown(KeyCode.R))
    //        setSendPowerLamp(powerLamp = !powerLamp);
    //    if (Input.GetKeyDown(KeyCode.T))
    //        setSendSelect1CrookLamp(select1CrookLamp = !select1CrookLamp);
    //    if (Input.GetKeyDown(KeyCode.Y))
    //        setSendSelect2CrookLamp(select2CrookLamp = !select2CrookLamp);
    //    if (Input.GetKeyDown(KeyCode.U))
    //        setSendBrakeFaultLamp(brakeFaultLamp = !brakeFaultLamp);
    //}
}
