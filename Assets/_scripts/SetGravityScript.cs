﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetGravityScript : MonoBehaviour
{
    [SerializeField] Rigidbody curRigidbody;

    private void OnEnable()
    {
        StartCoroutine(EnableGravity());
    }

    IEnumerator EnableGravity() {
        yield return new WaitForSeconds(1);
        curRigidbody.useGravity = true;
        this.enabled = false;
    }
}
