﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SetupScript : MonoBehaviour
{
    
    [SerializeField] Camera userDisplay1;
    [SerializeField] Camera userDisplay2;
    [SerializeField] Camera adminDisplay;

    [SerializeField] InputField comPortText;

    [SerializeField] Slider angleDisplay1Slider;
    [SerializeField] Slider angleDisplay2Slider;

    [SerializeField] Text angleDisplay1Text;
    [SerializeField] Text angleDisplay2Text;

    [SerializeField] Slider fovDisplay1Slider;
    [SerializeField] Slider fovDisplay2Slider;

    [SerializeField] Text fovDisplay1Text;
    [SerializeField] Text fovDisplay2Text;

    [SerializeField] Text versionAppText;

    [SerializeField] GameObject exitAppWindow;

    void Awake()
    {
        GetAngleDisplay1();
        GetAngleDisplay2();
        GetFOVDisplay1();
        GetFOVDisplay2();
        GetComPort();
        GetVersionApp();
    }

    public void SetComPort() {
        PlayerPrefs.SetInt("comPort", int.Parse(comPortText.text));
        SceneManager.LoadScene(0);
    }

    public void SetAngleDisplay1() {
        userDisplay1.transform.eulerAngles = new Vector3(angleDisplay1Slider.value, userDisplay1.transform.eulerAngles.y, userDisplay1.transform.eulerAngles.z);
        adminDisplay.transform.eulerAngles = new Vector3(angleDisplay1Slider.value, userDisplay1.transform.eulerAngles.y, userDisplay1.transform.eulerAngles.z);
        PlayerPrefs.SetFloat("angleDisplay1", angleDisplay1Slider.value);

        angleDisplay1Text.text = angleDisplay1Slider.value.ToString();
    }

    public void SetAngleDisplay2()
    {
        userDisplay2.transform.eulerAngles = new Vector3(angleDisplay2Slider.value, userDisplay2.transform.eulerAngles.y, userDisplay2.transform.eulerAngles.z);
        PlayerPrefs.SetFloat("angleDisplay2", angleDisplay2Slider.value);

        angleDisplay2Text.text = angleDisplay2Slider.value.ToString();
    }

    public void SetFOVDisplay1()
    {
        userDisplay1.fieldOfView = fovDisplay1Slider.value;
        adminDisplay.fieldOfView = fovDisplay1Slider.value;
        PlayerPrefs.SetFloat("fovDisplay1", fovDisplay1Slider.value);

        fovDisplay1Text.text = fovDisplay1Slider.value.ToString();
    }

    public void SetFOVDisplay2()
    {
        userDisplay2.fieldOfView = fovDisplay2Slider.value;
        PlayerPrefs.SetFloat("fovDisplay2", fovDisplay2Slider.value);

        fovDisplay2Text.text = fovDisplay2Slider.value.ToString();
    }

    private void GetAngleDisplay1()
    {
        if (PlayerPrefs.HasKey("angleDisplay1")) {
            angleDisplay1Slider.value = PlayerPrefs.GetFloat("angleDisplay1");
        }

        userDisplay1.transform.eulerAngles = new Vector3(angleDisplay1Slider.value, userDisplay1.transform.eulerAngles.y, userDisplay1.transform.eulerAngles.z);
        adminDisplay.transform.eulerAngles = new Vector3(angleDisplay1Slider.value, userDisplay1.transform.eulerAngles.y, userDisplay1.transform.eulerAngles.z);

        angleDisplay1Text.text = angleDisplay1Slider.value.ToString();
    }

    private void GetAngleDisplay2()
    {
        if (PlayerPrefs.HasKey("angleDisplay2"))
        {
            angleDisplay2Slider.value = PlayerPrefs.GetFloat("angleDisplay2");
        }

        userDisplay2.transform.eulerAngles = new Vector3(angleDisplay2Slider.value, userDisplay2.transform.eulerAngles.y, userDisplay2.transform.eulerAngles.z);

        angleDisplay2Text.text = angleDisplay2Slider.value.ToString();
    }

    private void GetFOVDisplay1()
    {
        if (PlayerPrefs.HasKey("fovDisplay1"))
        {
            fovDisplay1Slider.value = PlayerPrefs.GetFloat("fovDisplay1");
        }

        userDisplay1.fieldOfView = fovDisplay1Slider.value;
        adminDisplay.fieldOfView = fovDisplay1Slider.value;

        fovDisplay1Text.text = fovDisplay1Slider.value.ToString();
    }

    private void GetFOVDisplay2()
    {
        if (PlayerPrefs.HasKey("fovDisplay2"))
        {
            fovDisplay2Slider.value = PlayerPrefs.GetFloat("fovDisplay2");
        }

        userDisplay2.fieldOfView = fovDisplay2Slider.value;

        fovDisplay2Text.text = fovDisplay2Slider.value.ToString();
    }

    public void GetComPort()
    {
        if (PlayerPrefs.HasKey("comPort"))
        {
            comPortText.text = PlayerPrefs.GetInt("comPort").ToString();
        }
        else {
            comPortText.text = 0.ToString();
        }
    }

    private void GetVersionApp()
    {
        //Debug.Log("Application Version : " + Application.version);
        versionAppText.text = "ver: " + Application.version.ToString();
    }

    public void ExitAppWindow() {
        exitAppWindow.SetActive(true);
    }

    public void ExitApp()
    {
        Application.Quit();
    }

    void Update()
    {
        if (Input.GetKey("escape"))
        {
            ExitAppWindow();
        }
    }

}
