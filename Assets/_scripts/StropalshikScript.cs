﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StropalshikScript : MonoBehaviour
{
    [SerializeField] Camera cam;
    [SerializeField] TrainingScript trainingScript;
    [SerializeField] NavAgentScript navAgentScript;
    [SerializeField] Animator animator;
    //[SerializeField] bool stropalshikStatus;
    [SerializeField] Transform curHook, markerHook, curObjectToMove;
    [SerializeField] GameObject curSystemHookToMoove;
    [SerializeField] float dist, distX, distY, distZ;

    [SerializeField] List<Transform> moduleObjects;

    [SerializeField] RectTransform stropalArrow;
    [SerializeField] GameObject stropalLeft, stropalRight;
    private float delta = 80;

    public bool traversaOn;

    private void Start()
    {
        StartCoroutine(SetCurHook());
    }

    public void SetInfoForStropalshik(Transform hook, Transform objectToMove, GameObject systemHookToMoove) {
        curHook = hook;
        
        curObjectToMove = objectToMove;
        curSystemHookToMoove = systemHookToMoove;
    }

    private void Update()
    {
        if (markerHook == null) {
            return;
        }

        if (curHook && curObjectToMove) {
            //dist = Vector2.Distance(new Vector2(curHook.position.x, curHook.position.z), new Vector2(curObjectToMove.position.x, curObjectToMove.position.z));
            dist = Vector2.Distance(new Vector2(markerHook.position.x, markerHook.position.z), new Vector2(curObjectToMove.position.x, curObjectToMove.position.z));
            distX = markerHook.position.x - curObjectToMove.position.x;
            distY = curHook.position.y - curObjectToMove.position.y;
            distZ = markerHook.position.z - curObjectToMove.position.z;

            if (dist <= 0.2f) {

                if (Mathf.Abs(distY) > 0.2f)
                {
                    if (curHook.position.y > curObjectToMove.position.y)
                    {
                        animator.SetBool("NeedLeft", false);
                        animator.SetBool("NeedRight", false);
                        animator.SetBool("NeedForward", false);
                        animator.SetBool("NeedStop", false);
                        animator.SetBool("NeedDown", true);
                        animator.SetBool("NeedUp", false);
                    }
                    else
                    {
                        animator.SetBool("NeedLeft", false);
                        animator.SetBool("NeedRight", false);
                        animator.SetBool("NeedForward", false);
                        animator.SetBool("NeedStop", false);
                        animator.SetBool("NeedDown", false);
                        animator.SetBool("NeedUp", true);
                    }
                }
                else {
                    animator.SetBool("NeedLeft", false);
                    animator.SetBool("NeedRight", false);
                    animator.SetBool("NeedForward", false);
                    animator.SetBool("NeedStop", false);
                    animator.SetBool("NeedDown", false);
                    animator.SetBool("NeedUp", false);
                    animator.SetTrigger("OK");
                }

                return;
            }

            if (Mathf.Abs(distX) > Mathf.Abs(distZ))
            {
                //animator.transform.eulerAngles = new Vector3(0, 175, 0);
                if (distX > 0)
                {
                    animator.SetBool("NeedForward", false);
                    animator.SetBool("NeedStop", true);
                    animator.SetBool("NeedLeft", false);
                    animator.SetBool("NeedRight", false);
                    animator.SetBool("NeedDown", false);
                    animator.SetBool("NeedUp", false);
                }
                else
                {
                    animator.SetBool("NeedForward", true);
                    animator.SetBool("NeedStop", false);
                    animator.SetBool("NeedLeft", false);
                    animator.SetBool("NeedRight", false);
                    animator.SetBool("NeedDown", false);
                    animator.SetBool("NeedUp", false);
                }
            }
            else
            {
                //animator.transform.eulerAngles = new Vector3(0, -90, 0);
                if (distZ > 0)
                {
                    animator.SetBool("NeedLeft", false);
                    animator.SetBool("NeedRight", true);
                    animator.SetBool("NeedForward", false);
                    animator.SetBool("NeedStop", false);
                    animator.SetBool("NeedDown", false);
                    animator.SetBool("NeedUp", false);
                }
                else
                {
                    animator.SetBool("NeedLeft", true);
                    animator.SetBool("NeedRight", false);
                    animator.SetBool("NeedForward", false);
                    animator.SetBool("NeedStop", false);
                    animator.SetBool("NeedDown", false);
                    animator.SetBool("NeedUp", false);
                }
            }

            navAgentScript.SetRotateAgent();
        }

        ShowHelp();
    }

    //получаем все объекты для перемещения и места для перемещения текущего модуля
    public void GetModuleObjects(Transform curModule, bool traversaEnabled)
    {
        moduleObjects.Clear();
        for (int i = 0; i < curModule.childCount; i++)
        {
            if (curModule.GetChild(i).tag == "MoveFromHook" || curModule.GetChild(i).tag == "targetPos" || curModule.GetChild(i).tag == "MoveFromTraversa")
            {
                moduleObjects.Add(curModule.GetChild(i));
            }
        }

        if (traversaEnabled)
        {
            SetPos("MoveFromTraversa");
            traversaOn = true;
        }
        else {
            SetPos("MoveFromHook");
            traversaOn = false;
        }
    }

    //задаем позицию для стропальщика
    public void SetPos(string tag) {

        for (int i = 0; i < moduleObjects.Count; i++)
        {
            //Debug.Log(moduleObjects[i].name + ": " + moduleObjects[i].tag + " == " + tag +" && "+ CheckObject(moduleObjects[i], tag));



            if (moduleObjects[i].tag == tag && CheckObject(moduleObjects[i], tag) == true) {
                //Debug.Log("size: " + moduleObjects[i].gameObject.GetComponent<Collider>().bounds.size);
                navAgentScript.transform.position = new Vector3(moduleObjects[i].position.x, navAgentScript.transform.position.y, moduleObjects[i].position.z + moduleObjects[i].gameObject.GetComponent<Collider>().bounds.size.z);
                curObjectToMove = moduleObjects[i];
                break;
            }
            
        }
    }

    //проверяем перед выбором
    private bool CheckObject(Transform curObject, string tag) {
        for (int i = 0; i < moduleObjects.Count; i++)
        {
            if (moduleObjects[i] != curObject && moduleObjects[i].tag != tag)
            {
                if (Vector2.Distance(new Vector2(curObject.position.x, curObject.position.z), new Vector2(moduleObjects[i].position.x, moduleObjects[i].position.z)) < 1)
                {
                    return false;
                }
            }
        }
        return true;
    }

    //устанавливаем текущий hook
    private IEnumerator SetCurHook() {
        float height = 1000;
        int curHookID = 0;
        for (int i = 0; i < trainingScript.hookDetects.Length; i++)
        {
            if (trainingScript.hookDetects[i].transform.position.y < height && trainingScript.hookDetects[i].isActiveAndEnabled) {
                height = trainingScript.hookDetects[i].transform.position.y;
                curHookID = i;
            }
        }
        curHook = trainingScript.hookDetects[curHookID].transform;
        markerHook = curHook.GetComponent<HookDetect>().markerForStropal;

        yield return new WaitForSeconds(2);

        StartCoroutine(SetCurHook());
    }

    //подсказка местоположения стропальщика (когда не виден)
    private void ShowHelp() {

        stropalArrow.transform.position = cam.WorldToScreenPoint(navAgentScript.transform.position);

        if (Mathf.Abs(stropalArrow.localPosition.x) > Screen.width / 2 - delta)
        {
            stropalArrow.gameObject.SetActive(true);
            stropalArrow.localPosition = new Vector2(Mathf.Clamp(stropalArrow.localPosition.x, (Screen.width / 2 - delta) * -1, Screen.width / 2 - delta), (Screen.height / 2 - delta * 3) * -1);
        }
        else
        {
            stropalArrow.gameObject.SetActive(false);
            return;
        }

        if (stropalArrow.localPosition.x > 0)
        {
            stropalLeft.SetActive(false);
            stropalRight.SetActive(true);
        }
        else {
            stropalLeft.SetActive(true);
            stropalRight.SetActive(false);
        }

    }
}
