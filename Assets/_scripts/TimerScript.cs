﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerScript : MonoBehaviour
{

    [SerializeField] TrainingScript trainingScript;

    private float timeRemaining = 10;
    private bool timerIsRunning = false;
    public Text[] timeText;

    void Update()
    {
        if (timerIsRunning)
        {
            if (timeRemaining > 0)
            {
                timeRemaining -= Time.deltaTime;
                DisplayTime(timeRemaining);
            }
            else
            {
                Debug.Log("Time has run out!");
                timeRemaining = 0;
                timerIsRunning = false;
                SetTimerColor(Color.red);
                trainingScript.StartNextModule();
            }
        }
    }

    void DisplayTime(float timeToDisplay)
    {
        timeToDisplay += 1;

        float minutes = Mathf.FloorToInt(timeToDisplay / 60);
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);

        for (int i = 0; i < timeText.Length; i++)
        {
            timeText[i].text = string.Format("{0:00}:{1:00}", minutes, seconds);
        }
    }

    public void StartTimer(float minutes) {
        timeRemaining = minutes * 60;
        SetTimerIsRunning(true);
        SetTimerColor(Color.green);
    }

    public void SetTimerIsRunning(bool status)
    {
        timerIsRunning = status;
    }

    public bool GetTimerIsRunning()
    {
        return timerIsRunning;
    }

    private void SetTimerColor(Color newColor) {
        
        for (int i = 0; i < timeText.Length; i++)
        {
            timeText[i].color = newColor;
        }
    }

}
