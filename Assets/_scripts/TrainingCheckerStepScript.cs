﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainingCheckerStepScript : MonoBehaviour
{
    [SerializeField] TrainingScriptCurParameters trainingCur;

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("Enter:" + other.name);
    }

    void OnTriggerExit(Collider other)
    {
        Debug.Log("Exit:" + other.name);
    }
}
