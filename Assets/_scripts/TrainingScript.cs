﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TrainingScript : MonoBehaviour
{
    public TimerScript timerScript;
    [SerializeField] MoveScript moveScript;
    [SerializeField] HeightScript heightScript;
    [SerializeField] TrainingScriptCurParameters[] trainingModuls;
    [SerializeField] Color[] TrainingListItemColor;
    [SerializeField] Transform TrainingListItem, TrainingListContent;
    private TrainingScriptListItemParameters curItemParameters;

    //для вывода ошибок пользовательского ввода данных
    [SerializeField] InputField FIOInputField;
    [SerializeField] Image ScrollViewTrainings;
    [SerializeField] string[] ErrorTexts;
    [SerializeField] Text ErrorText;

    [SerializeField] List<TrainingScriptListItemParameters> trainingModulsList;
    [SerializeField] List<TrainingScriptListItemParameters> trainingModulsSelected;

    [SerializeField] int curModule;
    [SerializeField] GameObject adminZoneWindow;

    [SerializeField] GameObject startButton, pauseButton, nextModuleButton, pauseWindow, finishWindow, traversa;
    [SerializeField] GameObject[] warningWindows;
    [SerializeField] GameObject[] traversaCables;

    [SerializeField] Transform[] hooks;
    public string[] warningMessages;
    [SerializeField] GameObject hooking;

    [SerializeField] GameObject bgNextModule;
    [SerializeField] Text textNextModule;
    [SerializeField] Animator animatorNextModule;

    [SerializeField] Text textStartTraining;
    [SerializeField] Animator animatorStartTraining;

    public HookDetect[] hookDetects;

    public StropalshikScript stropalshikScript;

    [SerializeField] Vector3 craneBeamStartPos, carriageStartPos;

    //сохраняем физ данные крюков на время их возращения в стартовую позицию
    private float[] bodyMass = new float[2];
    private float[] bodyDrag = new float[2];
    private float[] bodyAngularDrag = new float[2];

    void Start()
    {
        SetTraversaVisibleCheck(false);
        BuildListOfTraining();
        adminZoneWindow.SetActive(true);
        animatorStartTraining.enabled = false;
        craneBeamStartPos = moveScript.craneBeam.localPosition;
        carriageStartPos = moveScript.carriage.localPosition;

        bodyMass = new float[hooks.Length];
        bodyDrag = new float[hooks.Length];
        bodyAngularDrag = new float[hooks.Length];
    }

    //строим список модулей
    private void BuildListOfTraining()
    {
        for (int i = 0; i < trainingModuls.Length; i++)
        {
            trainingModuls[i].UpdateModuleDate();

            Transform curTraining = Instantiate(TrainingListItem);
            curTraining.SetParent(TrainingListContent);

            curItemParameters = curTraining.GetComponent<TrainingScriptListItemParameters>();

            curItemParameters.NumberLoc = i;

            curItemParameters.Number.text = (i + 1).ToString();
            curItemParameters.TrainingName.text = trainingModuls[i].TrainingName;
            curItemParameters.TrainingTime.text = trainingModuls[i].TrainingTime.ToString();
            curItemParameters.TrainingEnabled.isOn = trainingModuls[i].TrainingEnabled;

            if (i % 2 != 0) {
                curItemParameters.Bg.color = TrainingListItemColor[1];
            }
            curItemParameters.gameObject.name = trainingModuls[i].TrainingNameLat;
            curTraining.gameObject.SetActive(true);
            trainingModulsList.Add(curItemParameters);
        }
    }

    //запускаем тренажер для выполнения
    public void StartTraining() {

        if (!CheckDataBeforStart()) {
            return;
        }
        //curModule = 0;
        StartModule();
        SetStatusTraining(true);
        MarkModuleInList();
        nextModuleButton.SetActive(true);
        //adminZoneWindow.SetActive(false);
    }

    private void StartModule() {
        StartCoroutine(ResetCranePos());
    }

    public void StartNextModule() {
        
        StartCoroutine(StartNextModuleCheck(false));
    }

    public void StartNextModuleFromAdminPanel()
    {
        StartCoroutine(StartNextModuleCheck(true));
    }

    IEnumerator StartNextModuleCheck(bool itsAdmin) {

        if (!itsAdmin)
        {
            yield return new WaitForSeconds(1);

            while (hooking.activeInHierarchy)
            {
                ShowWarning(true, warningMessages[1], 0);
                yield return null;
            }

            if (trainingModuls[trainingModulsSelected[curModule].NumberLoc].positionCountCur >= trainingModuls[trainingModulsSelected[curModule].NumberLoc].positionCountToFinish)
            {
                curModule++;

                if (curModule >= trainingModulsSelected.Count)
                {
                    SetEndTraining();
                }
                else
                {
                    StartModule();
                    MarkModuleInList();
                }
                
            }
        }
        else {
            curModule++;
            if (curModule >= trainingModulsSelected.Count)
            {
                SetEndTraining();
            }
            else
            {
                StartModule();
                MarkModuleInList();
            }
        }
        heightScript.CleareObjectsToMoove();
        CleareSystemHooks();
        ShowWarning(false, warningMessages[1], 0);
    }

    //выводим админу текущий модуль в списке
    private void MarkModuleInList() {
        for (int i = 0; i < trainingModulsSelected.Count; i++)
        {
            if (i == curModule) {
                trainingModulsSelected[i].Border.SetActive(true);
            } else {
                trainingModulsSelected[i].Border.SetActive(false);
            }
            
        }
    }

    private void SetEndTraining() {
        Debug.Log("END!");
        finishWindow.SetActive(true);
        timerScript.SetTimerIsRunning(false);

        startButton.SetActive(false);
        pauseButton.SetActive(false);
    }

    //проверяем введенные данные перед стартом
    private bool CheckDataBeforStart() {

        //введена ли ФИО студента
        ErrorText.text = "";
        if (FIOInputField.text == "")
        {
            SetErrorColor(FIOInputField.GetComponent<Image>(), false);
            GetTextError(0);
            return false;
        }
        else {
            SetErrorColor(FIOInputField.GetComponent<Image>(), true);
        }

        //выбран ли хоть один модуль для выполнения
        trainingModulsSelected.Clear();
        for (int i = 0; i < TrainingListContent.childCount; i++)
        {
            if (!TrainingListContent.GetChild(i).gameObject.activeInHierarchy) {
                continue;
            }
            curItemParameters = TrainingListContent.GetChild(i).GetComponent<TrainingScriptListItemParameters>();
            if (curItemParameters.TrainingEnabled.isOn) {
                trainingModulsSelected.Add(curItemParameters);
            }
        }

        if (trainingModulsSelected.Count == 0)
        {
            SetErrorColor(ScrollViewTrainings, false);
            GetTextError(1);
            return false;
        }
        else {
            SetErrorColor(ScrollViewTrainings, true);
        }

        return true;
    }

    //получаем текст для сообщения об ошибке
    private void GetTextError(int curError) {
        for (int i = 0; i < ErrorTexts.Length; i++)
        {
            if (curError == i) {
                ErrorText.text = ErrorText.text + ErrorTexts[i] + "\n";
                ShowTextError(true);
                return;
            }
        }
    }

    //показываем сообщение об ошибке
    private void ShowTextError(bool status) {
        ErrorText.gameObject.SetActive(status);
    }

    //выделяем красным цветом место с ошибками ввода
    private void SetErrorColor(Image curObject, bool status) {
        if (status)
        {
            curObject.color = Color.white;
        }
        else {
            curObject.color = Color.red;
        }
    }

    //переключаем вид кнопки старт/пауза
    public void SetStatusTraining(bool status) {
        startButton.SetActive(!status);
        pauseButton.SetActive(status);
        timerScript.SetTimerIsRunning(status);
        pauseWindow.SetActive(!status);
        if (!status) {
            startButton.GetComponentInChildren<Text>().text = "Продовжити";
        }
    }

    //проверка вкл/выкл траверсу
    public void SetTraversaVisibleCheck(bool status)
    {
        if (traversa != null)
        {
            if (!status)
            {
                SetTraversaVisible(false);
            }
            else {
                StartCoroutine(SetTraversaVisibleIE());
            }
        }
    }

    //вкл/выкл траверсу
    public void SetTraversaVisible(bool status)
    {
        if (status) {
            traversa.transform.position = new Vector3((hooks[0].position.x + hooks[1].position.x) / 2, hooks[0].position.y - 0.73f, (hooks[0].position.z + hooks[1].position.z) / 2 + 0.08f);
        }
        traversa.SetActive(status);
        for (int i = 0; i < traversaCables.Length; i++)
        {
            traversaCables[i].SetActive(status);
        }
    }

    IEnumerator SetTraversaVisibleIE() {
        while (Mathf.Abs(hooks[0].position.y - hooks[1].position.y) > 0.05f)
        {
            ShowWarning(true, warningMessages[0], 0);
            yield return null;
        }
        SetTraversaVisible(true);
        ShowWarning(false, warningMessages[0], 0);
    }

    //показываем дополнительные уведомления студенту
    public void ShowWarning(bool status, string message, float timer) {

        for (int i = 0; i < warningWindows.Length; i++)
        {
            warningWindows[i].GetComponentInChildren<Text>().text = message;
            warningWindows[i].SetActive(status);
        }

        if (timer > 0) {
            StartCoroutine(ShowWarningHide(timer));
        }
    }

    IEnumerator ShowWarningHide(float timer) {
        yield return new WaitForSeconds(timer);
        
        for (int i = 0; i < warningWindows.Length; i++)
        {
            warningWindows[i].SetActive(false);
        }
    }

    //сохраняем новые введенные данные на будущее
    public void SaveSetupParameters() {
        for (int i = 0; i < trainingModuls.Length; i++)
        {

            PlayerPrefs.SetString(trainingModuls[i].TrainingNameLat + "_Name", trainingModulsList[i].TrainingName.text);
            PlayerPrefs.SetFloat(trainingModuls[i].TrainingNameLat + "_Time", float.Parse(trainingModulsList[i].TrainingTime.text.Replace(".", ",")));

            int enabled = 1;
            if (!trainingModulsList[i].TrainingEnabled.isOn) {
                enabled = 0;
            }
            PlayerPrefs.SetInt(trainingModuls[i].TrainingNameLat + "_Enabled", enabled);

            PlayerPrefs.Save();

            trainingModuls[i].UpdateModuleDate();
        }
    }

    //следующий студент
    public void NextUser() {
        SceneManager.LoadScene(0);
    }

    //сбрасываем параметры позиции крюков для следующего модуля
    private IEnumerator ResetCranePos()
    {
        timerScript.SetTimerIsRunning(false);

        SetHookRigidbody(1);

        //Debug.Log("ResetCranePos!");
        if (trainingModulsSelected[curModule].NumberLoc != 0) {
            moveScript.panelScript.PowerStatus = false;
        }

        if (curModule == 0) {
            SetStartWindows();
        } else {
            bgNextModule.SetActive(true);
            textNextModule.text = "ЗАВДАННЯ НАСТУПНОГО МОДУЛЮ:\n\n'" + trainingModuls[trainingModulsSelected[curModule].NumberLoc].TrainingName + "'";

            for (int i = 0; i < hooks.Length; i++)
            {
                while (hooks[i].position.y < moveScript.moovePointY[1].position.y)
                {
                    for (int ii = 0; ii < hooks.Length; ii++)
                    {
                        if (hooks[ii].position.y < moveScript.moovePointY[1].position.y) {
                            moveScript.hawsersLeft[ii].Rotate(1 * Vector3.forward * (150 * Time.deltaTime));
                            moveScript.hawsersRight[ii].Rotate(1 * Vector3.back * (150 * Time.deltaTime));
                        }

                    }

                    moveScript.craneBeam.localPosition = Vector3.MoveTowards(moveScript.craneBeam.localPosition, craneBeamStartPos, 1.5f * Time.deltaTime);
                    moveScript.carriage.localPosition = Vector3.MoveTowards(moveScript.carriage.localPosition, carriageStartPos, 1.5f * Time.deltaTime);

                    yield return null;
                }
            }
        }

        yield return new WaitForSeconds(3);

        SetHookRigidbody(0);

        for (int i = 0; i < trainingModuls.Length; i++)
        {
            if (i == trainingModulsSelected[curModule].NumberLoc)
            {
                trainingModuls[i].gameObject.SetActive(true);
            }
            else
            {
                trainingModuls[i].gameObject.SetActive(false);
            }
        }

        if (curModule != 0)
        {
            while (animatorNextModule.GetCurrentAnimatorStateInfo(0).normalizedTime < 1)
            {
                yield return null;
            }
            bgNextModule.SetActive(false);
        }

        if (trainingModulsSelected[curModule].NumberLoc != 0)
        {
            moveScript.panelScript.PowerStatus = true;
        }
    }

    //задаем параметры стартового экрана для студента
    private void SetStartWindows() {
        textStartTraining.text = "ЗАВДАННЯ МОДУЛЮ:\n\n'" + trainingModuls[trainingModulsSelected[curModule].NumberLoc].TrainingName + "'";
        animatorStartTraining.enabled = true;
    }

    //выводим уведомления про удары об объекты
    public void SetImpactWarning() {
        ShowWarning(true, warningMessages[3], 1);
    }

    //выключаем все зацепы
    private void CleareSystemHooks() {
        for (int i = 0; i < hookDetects.Length; i++)
        {
            for (int ii = 0; ii < hookDetects[i].systemHookToMoove.Length; ii++)
            {
                hookDetects[i].systemHookToMoove[ii].SetActive(false);
            }
        }
    }

    //если перемещаем груз очень низко - вывод сообщения
    public void CheckMoveHeight() {
        if (heightScript.height < 0.5f && !warningWindows[0].activeInHierarchy) {
            ShowWarning(true, warningMessages[4], 2);
        }
    }

    //устанавливаем кастомные параметры крюков для перемещения их в ноль
    private void SetHookRigidbody(int id = 0) {
        for (int i = 0; i < hooks.Length; i++)
        {
            Rigidbody hookR = hooks[i].GetComponent<Rigidbody>();
            if (hookR) {

                if (bodyDrag[0] == 0) {
                    bodyMass[0] = hookR.mass;
                    bodyDrag[0] = hookR.drag;
                    bodyAngularDrag[0] = hookR.angularDrag;

                    bodyMass[1] = 1;
                    bodyDrag[1] = 1;
                    bodyAngularDrag[1] = 1;
                }

                hookR.mass = bodyMass[id];
                hookR.drag = bodyDrag[id];
                hookR.angularDrag = bodyAngularDrag[id];

                CapsuleCollider hookC = hooks[i].GetComponent<CapsuleCollider>();
                if (hookC) {
                    if (id == 1)
                    {
                        hookC.enabled = false;
                    }
                    else
                    {
                        hookC.enabled = true;
                    }
                }

            }
        }
        Rigidbody traversaR = traversa.GetComponent<Rigidbody>();
        traversaR.mass = bodyMass[id];
        traversaR.drag = bodyDrag[id];
        traversaR.angularDrag = bodyAngularDrag[id];

        MeshCollider hookT = traversa.GetComponent<MeshCollider>();
        if (hookT)
        {
            if (id == 1)
            {
                hookT.enabled = false;
            }
            else
            {
                hookT.enabled = true;
            }
        }

    }

}
