﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainingScriptCurParameters : MonoBehaviour
{
    public TrainingScript trainingScript;
    public string TrainingName;
    public float TrainingTime;
    public bool TrainingEnabled;
    public string TrainingNameLat;
    public bool TraversaEnabled;

    public int positionCountToFinish;
    public int positionCountCur;

    // обновляем данные модулей
    public void UpdateModuleDate() {

        //Debug.Log("UpdateModuleDate " + gameObject.name);

        if (PlayerPrefs.HasKey(TrainingNameLat + "_Name")) {
            TrainingName = PlayerPrefs.GetString(TrainingNameLat + "_Name");
        }

        if (PlayerPrefs.HasKey(TrainingNameLat + "_Time"))
        {
            TrainingTime = PlayerPrefs.GetFloat(TrainingNameLat + "_Time");
        }

        if (PlayerPrefs.HasKey(TrainingNameLat + "_Enabled"))
        {
            int enabled = PlayerPrefs.GetInt(TrainingNameLat + "_Enabled");
            if (enabled == 1)
            {
                TrainingEnabled = true;
            }
            else
            {
                TrainingEnabled = false;
            }
        }
    }

    private void OnEnable()
    {
        trainingScript.timerScript.StartTimer(TrainingTime);
        trainingScript.stropalshikScript.GetModuleObjects(transform, TraversaEnabled);
        //перевели вкл/выкл траверсы на приборную панель
        //trainingScript.SetTraversaVisibleCheck(TraversaEnabled);
    }

    private void OnDisable()
    {
        trainingScript.timerScript.SetTimerIsRunning(false);
    }

    //вызываем при перемещении груза в нужную позицию
    public void SetPositionCount(int addPoint) {
        positionCountCur += addPoint;
        CheckEndModule();
    }

    //проверяем все ли грузы на своих позициях
    private void CheckEndModule() {
        if(positionCountCur >= positionCountToFinish) {
            trainingScript.StartNextModule();    
        }
    }

}
