﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TrainingScriptListItemParameters : MonoBehaviour
{
    public int NumberLoc;
    public Text Number;
    public InputField TrainingName;
    public InputField TrainingTime;
    public Toggle TrainingEnabled;
    public Image Bg;
    public GameObject Border;
}
